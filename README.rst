========
atlasify
========


The Python package `atlasify` applies the ATLAS style to matplotlib plots. This includes

 - Switching to Arial font (not Helvetica since it's not widely available),
 - Adding ticks on all edges,
 - Making ticks to inward,
 - Adding the ***ATLAS*** badge with optional labels (e.g. Internal),
 - Adding a description below the badge, and
 - Moving the ***ATLAS*** badge outside the axes area.

Quickstart
==========

The package will use Helvetica. The
package ships with GPL-licensed Nimbus Sans L as a fallback.

The `atlasify` package can be installed using pip.

.. code::

   pip install atlasify
   # or 
   pip install https://gitlab.cern.ch/fsauerbu/atlasify/-/archive/master/atlasify-master.tar.gz

Indices and tables
==================
* `Git repository <https://gitlab.cern.ch/fsauerbu/atlasify>`_
* `PyPI repository <https://pypi.org/project/atlasify/>`_
* `Documentation <https://atlasify.readthedocs.org>`_
